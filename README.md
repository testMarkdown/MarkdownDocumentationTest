# Dialogs Markdown documentation testsss

Project created to test JSDoc and documentation.js as documentatation tools 

<a name="confirm"></a>

## confirm(title, message, confirm, cancel)
documented as $.dialog.confirm
This method creates a dialog box with a title, a message and 'confirm' and 'cancel' options.

**Kind**: global function  

| Param | Type | Description |
| --- | --- | --- |
| title | <code>string</code> | [The title of the dialog box] |
| message | <code>string</code> | [The message that will be displayed on the body of the dialog box] |
| confirm | <code>function</code> | [The function that will be called if the user selects the 'confirm' option] |
| cancel | <code>function</code> | [The function that will be called if the user selects the 'cancel' option] |

**Example**  
```js
$.dialog.confirm("Confirmação", "Tem certeza que deseja sair? Os dados não salvos serão descartados.", function() {
	$.dialog.close();
	window.history.back();
		});
```






















<!-- Generated by documentation.js. Update this documentation by updating the source code. -->

### Table of Contents

-   [$][1]
-   [ClassA][2]
    -   [Parameters][3]
-   [doSomething][4]
-   [doSomething][5]
    -   [Parameters][6]
    -   [Examples][7]

## $

Documentation os the dialogs.js API

## ClassA

Diese Klasse bla bla...

### Parameters

-   `type`  

## doSomething

This function does something

## doSomething

This method creates a dialog box with a title, a message and 'confirm' and 'cancel' options.

### Parameters

-   `title` **[string][8]** [The title of the dialog box]
-   `message` **[string][8]** [The message that will be displayed on the body of the dialog box]
-   `confirm` **[function][9]** [The function that will be called if the user selects the 'confirm' option]
-   `cancel` **[function][9]** [The function that will be called if the user selects the 'cancel' option]

### Examples

```javascript
// returns 2
$.dialog.confirm("", "Tem certeza que deseja sair? Os dados não salvos serão descartados.", function() {
	$.dialog.close();
	window.history.back();
});
```

[1]: #

[2]: #classa

[3]: #parameters

[4]: #dosomething

[5]: #dosomething-1

[6]: #parameters-1

[7]: #examples

[8]: https://developer.mozilla.org/docs/Web/JavaScript/Reference/Global_Objects/String

[9]: https://developer.mozilla.org/docs/Web/JavaScript/Reference/Statements/function


-------------------------------------------------------------------------------------------
<a name="$"></a>

## $ : <code>object</code>
Describe the namespace.

**Kind**: global namespace  
<a name="$.this.confirm"></a>

### $.this.confirm(title, message, confirm, cancel)
documented as $.dialog.confirm
This method creates a dialog box with a title, a message and 'confirm' and 'cancel' options.

**Kind**: static method of [<code>$</code>](#$)  

| Param | Type | Description |
| --- | --- | --- |
| title | <code>string</code> | [The title of the dialog box] |
| message | <code>string</code> | [The message that will be displayed on the body of the dialog box] |
| confirm | <code>function</code> | [The function that will be called if the user selects the 'confirm' option] |
| cancel | <code>function</code> | [The function that will be called if the user selects the 'cancel' option] |

**Example**  
```js
$.dialog.confirm("Confirmação", "Tem certeza que deseja sair? Os dados não salvos serão descartados.", function() {
	$.dialog.close();
	window.history.back();
		});
```






## Functions  -------//////////-////////////////

<dl>
<dt><a href="#waiting">waiting(m)</a></dt>
<dd><p>This method creates a dialog box with just a message and no interaction.
<a href="http://usejsdoc.org/tags-inline-link.html">link text</a></p>
</dd>
<dt><a href="#confirm">confirm(title, message, confirm, cancel)</a></dt>
<dd><p>This method creates a dialog box with a title, a message and &#39;confirm&#39; and &#39;cancel&#39; options.</p>
</dd>
<dt><a href="#prompt">prompt(title, m, label, selector, func)</a></dt>
<dd><p>This method creates a prompt box with a title, a message, a label, a selector and one function.</p>
</dd>
<dt><a href="#info">info(title, m, func)</a></dt>
<dd><p>This method creates a dialog box with a title, a message and one function.</p>
</dd>
</dl>

<a name="waiting"></a>

## waiting(m)
This method creates a dialog box with just a message and no interaction.
[link text](http://usejsdoc.org/tags-inline-link.html)

**Kind**: global function  

| Param | Type | Description |
| --- | --- | --- |
| m | <code>string</code> | [The message that will be displayed within the dialog box] |

<a name="confirm"></a>

## confirm(title, message, confirm, cancel)
This method creates a dialog box with a title, a message and 'confirm' and 'cancel' options.

**Kind**: global function  

| Param | Type | Description |
| --- | --- | --- |
| title | <code>string</code> | [The title of the dialog box] |
| message | <code>string</code> | [The message that will be displayed on the body of the dialog box] |
| confirm | <code>function</code> | [The function that will be called if the user selects the 'confirm' option] |
| cancel | <code>function</code> | [The function that will be called if the user selects the 'cancel' option] |

**Example**  
```js
$.dialog.confirm("Confirmação", "Tem certeza que deseja sair? Os dados não salvos serão descartados.", function() {
	$.dialog.close();
	window.history.back();
		});
```
<a name="prompt"></a>

## prompt(title, m, label, selector, func)
This method creates a prompt box with a title, a message, a label, a selector and one function.

**Kind**: global function  

| Param | Type | Description |
| --- | --- | --- |
| title | <code>string</code> | [The title of the dialog box] |
| m | <code>string</code> | [The message that will be displayed on the body of the dialog box] |
| label | <code>string</code> | [The label of the dialog] |
| selector | <code>string</code> | [String that will determinate the type of the input displayed within the prompt box] |
| func | <code>function</code> | [The function that will be called if the user selects the 'OK' option] |

<a name="info"></a>

## info(title, m, func)
This method creates a dialog box with a title, a message and one function.

**Kind**: global function  

| Param | Type | Description |
| --- | --- | --- |
| title | <code>string</code> | [The title of the dialog box] |
| m | <code>string</code> | [The message that will be displayed on the body of the dialog box] |
| func | <code>string</code> | [The function that will be called if the user selects to close the modal] |



## Functions ->__>_>_>_>_>>__>_>_>>_

<dl>
<dt><a href="#waiting">waiting(m)</a></dt>
<dd><p>This method creates a dialog box with just a message and no interaction.
<a href="http://usejsdoc.org/tags-inline-link.html">link text</a></p>
</dd>
<dt><a href="#confirm">confirm(title, message, confirm, cancel)</a></dt>
<dd><p>This method creates a dialog box with a title, a message and &#39;confirm&#39; and &#39;cancel&#39; options.</p>
</dd>
<dt><a href="#prompt">prompt(title, m, label, selector, func)</a></dt>
<dd><p>This method creates a prompt box with a title, a message, a label, a selector and one function.</p>
</dd>
<dt><a href="#info">info(title, m, func)</a></dt>
<dd><p>This method creates a dialog box with a title, a message and one function.</p>
</dd>
</dl>

<a name="waiting"></a>

## waiting(m)
This method creates a dialog box with just a message and no interaction.
[link text](http://usejsdoc.org/tags-inline-link.html)

**Kind**: global function  

| Param | Type | Description |
| --- | --- | --- |
| m | <code>string</code> | [The message that will be displayed within the dialog box] |

<a name="confirm"></a>

## confirm(title, message, confirm, cancel)
This method creates a dialog box with a title, a message and 'confirm' and 'cancel' options.

**Kind**: global function  

| Param | Type | Description |
| --- | --- | --- |
| title | <code>string</code> | [The title of the dialog box] |
| message | <code>string</code> | [The message that will be displayed on the body of the dialog box] |
| confirm | <code>function</code> | [The function that will be called if the user selects the 'confirm' option] |
| cancel | <code>function</code> | [The function that will be called if the user selects the 'cancel' option] |

**Example**  
```js
$.dialog.confirm("Confirmação", "Tem certeza que deseja sair? Os dados não salvos serão descartados.", function() {
	$.dialog.close();
	window.history.back();
		});
```
<a name="prompt"></a>

## prompt(title, m, label, selector, func)
This method creates a prompt box with a title, a message, a label, a selector and one function.

**Kind**: global function  

| Param | Type | Description |
| --- | --- | --- |
| title | <code>string</code> | [The title of the dialog box] |
| m | <code>string</code> | [The message that will be displayed on the body of the dialog box] |
| label | <code>string</code> | [The label of the dialog] |
| selector | <code>string</code> | [String that will determinate the type of the input displayed within the prompt box] |
| func | <code>function</code> | [The function that will be called if the user selects the 'OK' option] |

<a name="info"></a>

## info(title, m, func)
This method creates a dialog box with a title, a message and one function.

**Kind**: global function  

| Param | Type | Description |
| --- | --- | --- |
| title | <code>string</code> | [The title of the dialog box] |
| m | <code>string</code> | [The message that will be displayed on the body of the dialog box] |
| func | <code>string</code> | [The function that will be called if the user selects to close the modal] |
